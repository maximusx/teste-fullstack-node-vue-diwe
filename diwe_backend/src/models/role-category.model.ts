import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    foreignKeys: {
      fkRoleCategoryRoleId: {
        name: 'fkRoleCategoryRoleId',
        entity: 'Role',
        entityKey: 'id',
        foreignKey: 'roleId',
        onUpdate: 'no action',
        onDelete: 'cascade',
      },
      fkRoleCategoryCategoryId: {
        name: 'fkRoleCategoryCategoryId',
        entity: 'Category',
        entityKey: 'id',
        foreignKey: 'categoryId',
        onUpdate: 'no action',
        onDelete: 'cascade',
      },
    },
  },
})
export class RoleCategory extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
    length: 20,
    mysql: {
      dataType: 'bigint',
    },
  })
  roleId: number;

  @property({
    type: 'number',
    id: true,
    required: true,
    length: 20,
    mysql: {
      dataType: 'bigint',
    },
  })
  categoryId: number;

  constructor(data?: Partial<RoleCategory>) {
    super(data);
  }
}

export interface RoleCategoryRelations {
  // describe navigational properties here
}

export type RoleCategoryWithRelations = RoleCategory & RoleCategoryRelations;
