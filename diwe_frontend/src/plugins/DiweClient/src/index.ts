import Vue from "vue";
import DiweClient from "./DiweClient";

export default DiweClient;

const install = DiweClient.install;

DiweClient.install = (Vue, args) => {
    install.call(DiweClient, Vue, args);
};

if (typeof window !== "undefined" && window.Vue) {
    window.Vue.use(DiweClient);
}
