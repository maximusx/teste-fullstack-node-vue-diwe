import Vue from "vue";
import { install } from "./install";
import OpenAPIClientAxios from "openapi-client-axios";
import { Client as DiweAPIClient } from "../../OpenAPIClient";

const defaultOptions = {
    definition: ''
};

export default class DiweClient {
    static install = install;

    private apps: Array<Vue>;
    private app?: Vue;
    private api: OpenAPIClientAxios;
    private diweApiClient: DiweAPIClient;
    options?: any;
    initialized = false;

    constructor(options: any = {}) {
        this.initialized = false;
        this.apps = [];
        this.diweApiClient = {} as DiweAPIClient;
        this.options = {...defaultOptions, ...options};
        this.api = new OpenAPIClientAxios(this.options);
    }

    start(options?: any) {
        if (typeof options == 'undefined') {
            options = {...defaultOptions, ...this.options};
        }
        this.api = new OpenAPIClientAxios(options);
        this.api.getClient<DiweAPIClient>().then(client => {
            this.diweApiClient = client;
            this.initialized = true;
        });
    }

    get apiClient(): DiweAPIClient {
        return this.diweApiClient;
    }

    async getClient(): Promise<DiweAPIClient> {
        if (! this.api) {
            this.start();
        }
        return this.api.getClient<DiweAPIClient>();
    }

    init(app: Vue /* Vue component instance */) {
        this.apps.push(app);

        app.$once("hook:destroyed", () => {
            const index = this.apps.indexOf(app);
            if (index > -1) this.apps.splice(index, 1);
            if (this.app === app) this.app = this.apps[0] || null;
        });

        if (this.app) {
            return;
        }

        this.app = app;
    }
}
