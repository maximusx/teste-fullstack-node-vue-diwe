import {Entity, hasMany, model, property} from '@loopback/repository';
import {Category} from './category.model';
import {RoleCategory} from './role-category.model';

@model()
export class Role extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
    length: 20,
    mysql: {
      dataType: 'bigint',
    },
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    index: {
      unique: true,
    },
  })
  name: string;

  @property({
    type: 'string',
    length: 500,
  })
  description?: string;

  @property({
    type: 'boolean',
    required: true,
    mysql: {
      default: 1,
    },
  })
  active: boolean;

  @hasMany(() => Category, {through: {
    model: () => RoleCategory,
    keyFrom: 'roleId',
    keyTo: 'categoryId',
  }})
  categories: Category[];

  constructor(data?: Partial<Role>) {
    super(data);
  }
}

export interface RoleRelations {
  // describe navigational properties here
}

export type RoleWithRelations = Role & RoleRelations;
