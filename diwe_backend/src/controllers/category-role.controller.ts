import {
  repository
} from '@loopback/repository';
import {CategoryRepository} from '../repositories';

export class CategoryRoleController {
  constructor(
    @repository(CategoryRepository) protected categoryRepository: CategoryRepository,
  ) { }

  /*@get('/categories/{id}/roles', {
    responses: {
      '200': {
        description: 'Array of Category has many Role through RoleCategory',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Role)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Role>,
  ): Promise<Role[]> {
    return this.categoryRepository.roles(id).find(filter);
  }

  @post('/categories/{id}/roles', {
    responses: {
      '200': {
        description: 'create a Role model instance',
        content: {'application/json': {schema: getModelSchemaRef(Role)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Category.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Role, {
            title: 'NewRoleInCategory',
            exclude: ['id'],
          }),
        },
      },
    }) role: Omit<Role, 'id'>,
  ): Promise<Role> {
    return this.categoryRepository.roles(id).create(role);
  }

  @patch('/categories/{id}/roles', {
    responses: {
      '200': {
        description: 'Category.Role PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Role, {partial: true}),
        },
      },
    })
    role: Partial<Role>,
    @param.query.object('where', getWhereSchemaFor(Role)) where?: Where<Role>,
  ): Promise<Count> {
    return this.categoryRepository.roles(id).patch(role, where);
  }

  @del('/categories/{id}/roles', {
    responses: {
      '200': {
        description: 'Category.Role DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Role)) where?: Where<Role>,
  ): Promise<Count> {
    return this.categoryRepository.roles(id).delete(where);
  }*/
}
