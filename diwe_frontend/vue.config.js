const path = require("path");
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");
const { WebpackPluginRamdisk } = require("webpack-plugin-ramdisk");
const { existsSync, rmdirSync } = require("fs");
const execa = require("execa");

const dirName = __dirname;
const ramDiskOptions = {
    blockSize: 512,
    // 256 mb
    //bytes: 2.56e8,
    bytes: 5.12e8,
    name: "diweBuild",
};
const mountPath = path.resolve("/mnt/", "./", ramDiskOptions.name);
const checkRamDisk = function() {
    try {
        if (existsSync(mountPath)) {
            try {
                const { stdout: isMounted } = execa.commandSync(
                    "sudo mount |awk '{print $3}'| grep -w " + `${mountPath}`,
                    {
                        shell: true,
                    }
                );
                if (!isMounted || !isMounted.trim()) {
                    rmdirSync(mountPath);
                }
            } catch (error) {
                rmdirSync(mountPath);
            }
        }
    } catch (error) {
        console.log(error);
        throw error;
    }
};

module.exports = {
    transpileDependencies: ["vuetify"],
    configureWebpack: config => {
        if (
            process.argv &&
            process.argv.length > 2 &&
            (process.argv[2] == "build" || process.argv[2] == "serve") &&
            process.env.NODE_ENV ==
                "local" /*||
                process.env.NODE_ENV == "development" ||
                process.env.NODE_ENV == "production"*/
        ) {
            checkRamDisk();
            const ramDiskPlugin = new WebpackPluginRamdisk(ramDiskOptions);
            const cachePrune = {
                // Caches younger than `maxAge` are not considered for deletion. They must
                // be at least this (default: 2 days) old in milliseconds.
                maxAge: 2 * 24 * 60 * 60 * 1000,
                // All caches together must be larger than `sizeThreshold` before any
                // caches will be deleted. Together they must be at least this
                // (default: 50 MB) big in bytes.
                sizeThreshold: 50 * 1024 * 1024,
            };

            if (process.env.npm_config_nocache) {
                cachePrune.maxAge = 1;
            }

            if (!(config.plugins instanceof Array)) {
                config.plugins = [];
            }
            config.plugins.push(
                new HardSourceWebpackPlugin({
                    info: {
                        mode: "none",
                        level: "debug",
                    },
                    cacheDirectory: path.resolve(
                        ramDiskPlugin.diskPath,
                        "./node_modules/.cache/hard-source/[confighash]"
                    ),
                    environmentHash: {
                        root: dirName,
                        directories: [],
                        files: ["package-lock.json", "yarn.lock"],
                    },
                    cachePrune: cachePrune,
                })
            );
        }
    },
};
