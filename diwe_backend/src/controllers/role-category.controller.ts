import {
  Filter,
  IsolationLevel,
  repository
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,

  param,

  post,
  requestBody
} from '@loopback/rest';
import {
  Category, Role,
  RoleCategory
} from '../models';
import {RoleCategoryRepository, RoleRepository} from '../repositories';

export class RoleCategoryController {
  constructor(
    @repository(RoleRepository) protected roleRepository: RoleRepository,
    @repository(RoleCategoryRepository) protected roleCategoryRepository: RoleCategoryRepository,
  ) { }

  @get('/roles/{id}/categories', {
    responses: {
      '200': {
        description: 'Array of Role has many Category through RoleCategory',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Category)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Category>,
  ): Promise<Category[]> {
    return this.roleRepository.categories(id).find(filter);
  }

  @post('/roles/{id}/categories', {
    responses: {
      '200': {
        description: 'create a Category model instance',
        content: {'application/json': {schema: getModelSchemaRef(Category, {partial: true}),}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Role.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema:  {
            type: 'array',
            items: getModelSchemaRef(RoleCategory,  {
              title: 'NewRoleCategory',
              exclude: ['roleId'],
            })
          }
        },
      },
    }) roleCategory: Array<RoleCategory>
  ): Promise<RoleCategory[]> {
    return new Promise((resolve, reject) => {
      this.roleRepository.categories(id).find().then(categories => {
        this.roleCategoryRepository.beginTransaction(IsolationLevel.READ_COMMITTED).then(tx => {
          let promisesDelete: any[] = [];
          let promisesCreate = [];
          if (categories &&
            categories.length > 0
          ) {
            promisesDelete = categories.map(cat => {
              return this.roleCategoryRepository.deleteAll({
                roleId: id,
                categoryId: cat.id,
              }, {transaction: tx}).then(res => {

              });
            });
          }
          if (roleCategory && roleCategory.length > 0) {
            const roleCategories = new Array<RoleCategory>();
            promisesCreate = roleCategory.map(cat => {
              return this.roleCategoryRepository.create({
                roleId: id,
                categoryId: cat.categoryId,
              }, {transaction: tx}).then(res => {
                roleCategories.push(new RoleCategory({
                  roleId: id,
                  categoryId: cat.categoryId,
                }));
              });
            });
            Promise.all(promisesDelete.concat(promisesCreate)).then(res => {
              tx.commit();
              resolve(roleCategories);
            }).catch(error => {
              tx.rollback();
              const err = new Error(error.sqlMessage ? error.sqlMessage : 'Error adding relation.');
              (err as any).statusCode = 422;
              reject(err);
            });
          } else {
            Promise.all(promisesDelete).then(res => {
              tx.commit();
              resolve([]);
            }).catch(error => {
              tx.rollback();
              const err = new Error(error.sqlMessage ? error.sqlMessage : 'Error adding relation.');
              (err as any).statusCode = 422;
              reject(err);
            });
          }
        }).catch(error => {
          const err = new Error(error.sqlMessage ? error.sqlMessage : 'Error adding relation.');
          (err as any).statusCode = 500;
          reject(err);
        });
      }).catch(error => {
        const err = new Error(error.sqlMessage ? error.sqlMessage : 'Error adding relation.');
        (err as any).statusCode = 500;
        reject(err);
      });
    });
  }
}
