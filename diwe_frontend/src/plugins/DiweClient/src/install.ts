import Vue, { VueConstructor } from "vue";

let _Vue: VueConstructor;

export function install(Vue: VueConstructor, args?: any) {
    if ((install as any).installed && _Vue === Vue) {
        return;
    }
    (install as any).installed = true;

    _Vue = Vue;

    Vue.mixin({
        beforeCreate() {
            const options = this.$options as any;
            if (options.loopbackApi) {
                options.loopbackApi.init(this);
                this.$loopbackApi = options.loopbackApi;
            } else {
                this.$loopbackApi =
                    (options.parent && options.parent.$loopbackApi) || this;
            }
        },
    });
}
