import Vue, { PluginFunction } from "vue";
import { Client as DiweAPIClient } from "../../OpenAPIClient";

export declare class DiweClient {
    constructor(options?: any = {});

    options?: any;
    initialized: boolean;

    start(options?: any);
    apiClient: DiweAPIClient;
    async getClient(): Promise<DiweAPIClient>;

    static install: PluginFunction<never>;
}
