import {Entity, model, property, hasMany} from '@loopback/repository';
import {Role} from './role.model';
import {RoleCategory} from './role-category.model';

@model()
export class Category extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
    length: 20,
    mysql: {
      dataType: 'bigint',
    },
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    index: {
      unique: true,
    },
  })
  name: string;

  @property({
    type: 'boolean',
    required: true,
    mysql: {
      default: 1,
    },
  })
  active: boolean;

  @hasMany(() => Role, {through: {model: () => RoleCategory}})
  roles: Role[];

  constructor(data?: Partial<Category>) {
    super(data);
  }
}

export interface CategoryRelations {
  // describe navigational properties here
}

export type CategoryWithRelations = Category & CategoryRelations;
