import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: "/",
        name: "Home",
        component: () =>
            import(/* webpackChunkName: "home" */ "../views/Home.vue"),
    },
    {
        path: "/categoria",
        name: "category",
        component: () =>
            import(/* webpackChunkName: "category" */ "../views/Category.vue"),
    },
    {
        path: "/funcao",
        name: "role",
        component: () =>
            import(/* webpackChunkName: "role" */ "../views/Role.vue"),
    },
    {
        path: "/funcao/:id",
        name: "role-category",
        component: () =>
            import(/* webpackChunkName: "role" */ "../views/RoleCategory.vue"),
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
