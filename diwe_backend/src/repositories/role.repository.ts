import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasManyThroughRepositoryFactory, repository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Category, Role, RoleCategory, RoleRelations} from '../models';
import {CategoryRepository} from './category.repository';
import {RoleCategoryRepository} from './role-category.repository';

export class RoleRepository extends DefaultCrudRepository<
  Role,
  typeof Role.prototype.id,
  RoleRelations
> {

  public roleCategoryRepository: RoleCategoryRepository;
  public readonly categories: HasManyThroughRepositoryFactory<Category, typeof Category.prototype.id,
          RoleCategory,
          typeof Role.prototype.id
        >;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('RoleCategoryRepository') protected roleCategoryRepositoryGetter: Getter<RoleCategoryRepository>,
    @repository.getter('CategoryRepository') protected categoryRepositoryGetter: Getter<CategoryRepository>,
  ) {
    super(Role, dataSource);
    roleCategoryRepositoryGetter().then(res => this.roleCategoryRepository = res);
    this.categories = this.createHasManyThroughRepositoryFactoryFor('categories', categoryRepositoryGetter, roleCategoryRepositoryGetter,);
    this.registerInclusionResolver('categories', this.categories.inclusionResolver);
  }
}
