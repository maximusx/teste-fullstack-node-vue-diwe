import Vue from "vue";
import DiweClient from "./DiweClient/src";

Vue.use(DiweClient);

const loopbackApi = new DiweClient({});

export default loopbackApi;
