import {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios'; 

declare namespace Components {
  namespace Schemas {
    /**
     * Category
     */
    export interface Category {
      id?: number;
      name: string;
      active: boolean;
    }
    /**
     * Category.Fields
     */
    export interface CategoryFields {
      id?: boolean;
      name?: boolean;
      active?: boolean;
    }
    /**
     * Category.Filter
     */
    export interface CategoryFilter {
      offset?: number;
      /**
       * example:
       * 100
       */
      limit?: number;
      skip?: number;
      order?: string | string[];
      fields?: CategoryFields;
      /**
       * Category.IncludeFilter
       */
      include?: CategoryIncludeFilterItems[];
    }
    /**
     * Category.Filter
     */
    export interface CategoryFilter1 {
      offset?: number;
      /**
       * example:
       * 100
       */
      limit?: number;
      skip?: number;
      order?: string | string[];
      /**
       * Category.WhereFilter
       */
      where?: {
        [name: string]: any;
      };
      fields?: CategoryFields;
      /**
       * Category.IncludeFilter
       */
      include?: CategoryIncludeFilterItems[];
    }
    /**
     * Category.IncludeFilter.Items
     */
    export interface CategoryIncludeFilterItems {
      relation?: string;
      scope?: CategoryScopeFilter;
    }
    /**
     * CategoryPartial
     * (tsType: Partial<Category>, schemaOptions: { partial: true })
     */
    export interface CategoryPartial {
      id?: number;
      name?: string;
      active?: boolean;
    }
    /**
     * Category.ScopeFilter
     */
    export interface CategoryScopeFilter {
      offset?: number;
      /**
       * example:
       * 100
       */
      limit?: number;
      skip?: number;
      order?: string | string[];
      where?: {
        [name: string]: any;
      };
      fields?: {
        [name: string]: any;
      };
      include?: {
        [name: string]: any;
      }[];
    }
    /**
     * CategoryWithRelations
     * (tsType: CategoryWithRelations, schemaOptions: { includeRelations: true })
     */
    export interface CategoryWithRelations {
      id?: number;
      name: string;
      active: boolean;
      roles?: RoleWithRelations[];
    }
    /**
     * loopback.Count
     */
    export interface LoopbackCount {
      count?: number;
    }
    /**
     * NewCategory
     * (tsType: Omit<Category, 'id'>, schemaOptions: { title: 'NewCategory', exclude: [ 'id' ] })
     */
    export interface NewCategory {
      name: string;
      active: boolean;
    }
    /**
     * NewRole
     * (tsType: Omit<Role, 'id'>, schemaOptions: { title: 'NewRole', exclude: [ 'id' ] })
     */
    export interface NewRole {
      name: string;
      description?: string;
      active: boolean;
    }
    /**
     * NewRoleCategory
     * (tsType: Omit<RoleCategory, 'roleId'>, schemaOptions: { title: 'NewRoleCategory', exclude: [ 'roleId' ] })
     */
    export interface NewRoleCategory {
      categoryId: number;
    }
    /**
     * PingResponse
     */
    export interface PingResponse {
      greeting?: string;
      date?: string;
      url?: string;
      headers?: {
        [name: string]: any;
        "Content-Type"?: string;
      };
    }
    /**
     * Role
     */
    export interface Role {
      id?: number;
      name: string;
      description?: string;
      active: boolean;
    }
    /**
     * Role.Fields
     */
    export interface RoleFields {
      id?: boolean;
      name?: boolean;
      description?: boolean;
      active?: boolean;
    }
    /**
     * Role.Filter
     */
    export interface RoleFilter {
      offset?: number;
      /**
       * example:
       * 100
       */
      limit?: number;
      skip?: number;
      order?: string | string[];
      fields?: RoleFields;
      /**
       * Role.IncludeFilter
       */
      include?: RoleIncludeFilterItems[];
    }
    /**
     * Role.Filter
     */
    export interface RoleFilter1 {
      offset?: number;
      /**
       * example:
       * 100
       */
      limit?: number;
      skip?: number;
      order?: string | string[];
      /**
       * Role.WhereFilter
       */
      where?: {
        [name: string]: any;
      };
      fields?: RoleFields;
      /**
       * Role.IncludeFilter
       */
      include?: RoleIncludeFilterItems[];
    }
    /**
     * Role.IncludeFilter.Items
     */
    export interface RoleIncludeFilterItems {
      relation?: string;
      scope?: RoleScopeFilter;
    }
    /**
     * RolePartial
     * (tsType: Partial<Role>, schemaOptions: { partial: true })
     */
    export interface RolePartial {
      id?: number;
      name?: string;
      description?: string;
      active?: boolean;
    }
    /**
     * Role.ScopeFilter
     */
    export interface RoleScopeFilter {
      offset?: number;
      /**
       * example:
       * 100
       */
      limit?: number;
      skip?: number;
      order?: string | string[];
      where?: {
        [name: string]: any;
      };
      fields?: {
        [name: string]: any;
      };
      include?: {
        [name: string]: any;
      }[];
    }
    /**
     * RoleWithRelations
     * (tsType: RoleWithRelations, schemaOptions: { includeRelations: true })
     */
    export interface RoleWithRelations {
      id?: number;
      name: string;
      description?: string;
      active: boolean;
      categories?: CategoryWithRelations[];
    }
  }
}
declare namespace Paths {
  namespace CategoryControllerCount {
    namespace Responses {
      export type $200 = Components.Schemas.LoopbackCount;
    }
  }
  namespace CategoryControllerCreate {
    export type RequestBody = Components.Schemas.NewCategory;
    namespace Responses {
      export type $200 = Components.Schemas.Category;
    }
  }
  namespace CategoryControllerDeleteById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
  }
  namespace CategoryControllerFind {
    namespace Responses {
      export type $200 = Components.Schemas.CategoryWithRelations[];
    }
  }
  namespace CategoryControllerFindById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.CategoryWithRelations;
    }
  }
  namespace CategoryControllerReplaceById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export type RequestBody = Components.Schemas.Category;
  }
  namespace CategoryControllerUpdateAll {
    export type RequestBody = Components.Schemas.CategoryPartial;
    namespace Responses {
      export type $200 = Components.Schemas.LoopbackCount;
    }
  }
  namespace CategoryControllerUpdateById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export type RequestBody = Components.Schemas.CategoryPartial;
  }
  namespace PingControllerPing {
    namespace Responses {
      export type $200 = Components.Schemas.PingResponse;
    }
  }
  namespace RoleCategoryControllerCreate {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export type RequestBody = Components.Schemas.NewRoleCategory[];
    namespace Responses {
      export type $200 = Components.Schemas.CategoryPartial;
    }
  }
  namespace RoleCategoryControllerFind {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.Category[];
    }
  }
  namespace RoleControllerCount {
    namespace Responses {
      export type $200 = Components.Schemas.LoopbackCount;
    }
  }
  namespace RoleControllerCreate {
    export type RequestBody = Components.Schemas.NewRole;
    namespace Responses {
      export type $200 = Components.Schemas.Role;
    }
  }
  namespace RoleControllerDeleteById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
  }
  namespace RoleControllerFind {
    namespace Responses {
      export type $200 = Components.Schemas.RoleWithRelations[];
    }
  }
  namespace RoleControllerFindById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.RoleWithRelations;
    }
  }
  namespace RoleControllerReplaceById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export type RequestBody = Components.Schemas.Role;
  }
  namespace RoleControllerUpdateAll {
    export type RequestBody = Components.Schemas.RolePartial;
    namespace Responses {
      export type $200 = Components.Schemas.LoopbackCount;
    }
  }
  namespace RoleControllerUpdateById {
    namespace Parameters {
      export type Id = number;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export type RequestBody = Components.Schemas.RolePartial;
  }
}

export interface OperationMethods {
  /**
   * CategoryController.count
   */
  'CategoryController.count'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CategoryControllerCount.Responses.$200>
  /**
   * CategoryController.findById
   */
  'CategoryController.findById'(
    parameters?: Parameters<Paths.CategoryControllerFindById.PathParameters>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CategoryControllerFindById.Responses.$200>
  /**
   * CategoryController.replaceById
   */
  'CategoryController.replaceById'(
    parameters?: Parameters<Paths.CategoryControllerReplaceById.PathParameters>,
    data?: Paths.CategoryControllerReplaceById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * CategoryController.updateById
   */
  'CategoryController.updateById'(
    parameters?: Parameters<Paths.CategoryControllerUpdateById.PathParameters>,
    data?: Paths.CategoryControllerUpdateById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * CategoryController.deleteById
   */
  'CategoryController.deleteById'(
    parameters?: Parameters<Paths.CategoryControllerDeleteById.PathParameters>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * CategoryController.find
   */
  'CategoryController.find'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CategoryControllerFind.Responses.$200>
  /**
   * CategoryController.create
   */
  'CategoryController.create'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: Paths.CategoryControllerCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CategoryControllerCreate.Responses.$200>
  /**
   * CategoryController.updateAll
   */
  'CategoryController.updateAll'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: Paths.CategoryControllerUpdateAll.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CategoryControllerUpdateAll.Responses.$200>
  /**
   * PingController.ping
   */
  'PingController.ping'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PingControllerPing.Responses.$200>
  /**
   * RoleController.count
   */
  'RoleController.count'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RoleControllerCount.Responses.$200>
  /**
   * RoleCategoryController.find
   */
  'RoleCategoryController.find'(
    parameters?: Parameters<Paths.RoleCategoryControllerFind.PathParameters>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RoleCategoryControllerFind.Responses.$200>
  /**
   * RoleCategoryController.create
   */
  'RoleCategoryController.create'(
    parameters?: Parameters<Paths.RoleCategoryControllerCreate.PathParameters>,
    data?: Paths.RoleCategoryControllerCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RoleCategoryControllerCreate.Responses.$200>
  /**
   * RoleController.findById
   */
  'RoleController.findById'(
    parameters?: Parameters<Paths.RoleControllerFindById.PathParameters>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RoleControllerFindById.Responses.$200>
  /**
   * RoleController.replaceById
   */
  'RoleController.replaceById'(
    parameters?: Parameters<Paths.RoleControllerReplaceById.PathParameters>,
    data?: Paths.RoleControllerReplaceById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * RoleController.updateById
   */
  'RoleController.updateById'(
    parameters?: Parameters<Paths.RoleControllerUpdateById.PathParameters>,
    data?: Paths.RoleControllerUpdateById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * RoleController.deleteById
   */
  'RoleController.deleteById'(
    parameters?: Parameters<Paths.RoleControllerDeleteById.PathParameters>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * RoleController.find
   */
  'RoleController.find'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RoleControllerFind.Responses.$200>
  /**
   * RoleController.create
   */
  'RoleController.create'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: Paths.RoleControllerCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RoleControllerCreate.Responses.$200>
  /**
   * RoleController.updateAll
   */
  'RoleController.updateAll'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: Paths.RoleControllerUpdateAll.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RoleControllerUpdateAll.Responses.$200>
}

export interface PathsDictionary {
  ['/categories/count']: {
    /**
     * CategoryController.count
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CategoryControllerCount.Responses.$200>
  }
  ['/categories/{id}']: {
    /**
     * CategoryController.replaceById
     */
    'put'(
      parameters?: Parameters<Paths.CategoryControllerReplaceById.PathParameters>,
      data?: Paths.CategoryControllerReplaceById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * CategoryController.updateById
     */
    'patch'(
      parameters?: Parameters<Paths.CategoryControllerUpdateById.PathParameters>,
      data?: Paths.CategoryControllerUpdateById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * CategoryController.findById
     */
    'get'(
      parameters?: Parameters<Paths.CategoryControllerFindById.PathParameters>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CategoryControllerFindById.Responses.$200>
    /**
     * CategoryController.deleteById
     */
    'delete'(
      parameters?: Parameters<Paths.CategoryControllerDeleteById.PathParameters>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/categories']: {
    /**
     * CategoryController.create
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: Paths.CategoryControllerCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CategoryControllerCreate.Responses.$200>
    /**
     * CategoryController.updateAll
     */
    'patch'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: Paths.CategoryControllerUpdateAll.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CategoryControllerUpdateAll.Responses.$200>
    /**
     * CategoryController.find
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CategoryControllerFind.Responses.$200>
  }
  ['/ping']: {
    /**
     * PingController.ping
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PingControllerPing.Responses.$200>
  }
  ['/roles/count']: {
    /**
     * RoleController.count
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RoleControllerCount.Responses.$200>
  }
  ['/roles/{id}/categories']: {
    /**
     * RoleCategoryController.create
     */
    'post'(
      parameters?: Parameters<Paths.RoleCategoryControllerCreate.PathParameters>,
      data?: Paths.RoleCategoryControllerCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RoleCategoryControllerCreate.Responses.$200>
    /**
     * RoleCategoryController.find
     */
    'get'(
      parameters?: Parameters<Paths.RoleCategoryControllerFind.PathParameters>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RoleCategoryControllerFind.Responses.$200>
  }
  ['/roles/{id}']: {
    /**
     * RoleController.replaceById
     */
    'put'(
      parameters?: Parameters<Paths.RoleControllerReplaceById.PathParameters>,
      data?: Paths.RoleControllerReplaceById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * RoleController.updateById
     */
    'patch'(
      parameters?: Parameters<Paths.RoleControllerUpdateById.PathParameters>,
      data?: Paths.RoleControllerUpdateById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * RoleController.findById
     */
    'get'(
      parameters?: Parameters<Paths.RoleControllerFindById.PathParameters>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RoleControllerFindById.Responses.$200>
    /**
     * RoleController.deleteById
     */
    'delete'(
      parameters?: Parameters<Paths.RoleControllerDeleteById.PathParameters>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/roles']: {
    /**
     * RoleController.create
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: Paths.RoleControllerCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RoleControllerCreate.Responses.$200>
    /**
     * RoleController.updateAll
     */
    'patch'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: Paths.RoleControllerUpdateAll.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RoleControllerUpdateAll.Responses.$200>
    /**
     * RoleController.find
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RoleControllerFind.Responses.$200>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
