import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import loopbackApi from "./plugins/loopbackApi";

Vue.config.devtools = true;
Vue.config.performance = true;
Vue.config.productionTip = false;

new Vue({
    router,
    vuetify,
    loopbackApi,
    render: h => h(App),
}).$mount("#app");
