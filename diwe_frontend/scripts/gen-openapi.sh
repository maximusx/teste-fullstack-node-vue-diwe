#!/bin/bash
set -e

NC='\033[0m'
RED='\033[0;31m'
GREEN='\033[1;32m'

echo -e "${GREEN}==================================================${NC}"
echo -e "${GREEN}===           CRIANDO OPENAPI CLIENT           ===${NC}"
echo -e "${GREEN}==================================================${NC}"

typegen http://api:${API_PORT}/openapi.yaml > ./src/plugins/OpenAPIClient.d.ts

echo -e "${RED}==================================================${NC}"
echo -e "${RED}===            CONCLUÍDO            ===${NC}"
echo -e "${RED}==================================================${NC}"
