import Vue from "vue";
import DiweClient from "./index";


declare module 'vue/types/vue' {
  export interface Vue {
    $loopbackApi: DiweClient
  }
}

declare module 'vue/types/options' {
  export interface ComponentOptions<
    V extends Vue,
    Data=DefaultData<V>,
    Methods=DefaultMethods<V>,
    Computed=DefaultComputed,
    PropsDef=PropsDefinition<DefaultProps>,
    Props=DefaultProps> {
      loopbackApi?: DiweClient
  }
}
