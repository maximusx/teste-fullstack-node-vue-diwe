import {inject} from '@loopback/core';
import {DefaultTransactionalRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {RoleCategory, RoleCategoryRelations} from '../models';

export class RoleCategoryRepository extends DefaultTransactionalRepository<
  RoleCategory,
  typeof RoleCategory.prototype.roleId,
  RoleCategoryRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(RoleCategory, dataSource);
  }
}
