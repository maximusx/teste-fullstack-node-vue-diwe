### Executando o Teste para desenvolvedor Fullstack Node.js / Vue.js
##
A instalação é feita com o Docker (docker-compose). Os serviços incluídos são:
- Mysql para banco de dados da API backend
- NodeJs para a API backend
- NodeJs/Nginx para o frontend

##
Antes de executar a instalação, confira as configurações no arquivo `.env` na raiz do repositório. Nele contém a porta de acesso padrão `8988` do frontend, a porta de acesso padrão `3001` da API backend e o hostname padrão da URL de acesso `diwe.frontend`, mude se necessário e insira no seu arquivo de hosts os endereços:

```
XXX.XXX.XXX.XXX HOST_NAME
XXX.XXX.XXX.XXX api.HOST_NAME
```

Por exemplo
```
192.168.2.3 diwe.frontend
192.168.2.3 api.diwe.frontend
```

##
Para executar a instalação pela primeira vez rode o seguinte comando:

`docker-compose up composer mysql api frontend`

Esteja atento para a mensagem de conclusão da criação/importação do banco de dados inicial:

![img](img.jpg)

Para executar novamente após a primeira execução rode o seguinte comando que irá iniciar todos os serviços necessários:

`docker-compose up frontend`
##
O acesso ao frontend é feito pelo endereço por padrão `http://diwe.frontend:8988`, confira caso tenha feito alguma alteração nas variáveis do arquivo `.env`.

O acesso à API, caso necessário, é feito pelo endereço por padrão `http://api.diwe.frontend:3001/`, confira caso tenha feito alguma alteração nas variáveis do arquivo `.env`.

Qualquer dúvida ou problemas sinta-se livre para entrar em contato.
#
# Teste para desenvolvedor Fullstack Node.js / Vue.js

Desenvolver uma RESTful API e uma pequena aplicação a partir do estudo de caso proposto

## Premissas
- Utilizar banco de dados relacional
- Utilizar algum framework do Node.js
- Modelagem do banco de dados com o desafio proposto
- Utilizar o Vue.js no frontend

## Diferenciais
- Documentação dos endpoints da API
- Utilizar o Vuetify
- Utilizar o framework Loopback
- Construção de componentes reutilizáveis

## Objetivo
O objetivo do desafio é construir uma API e uma pequena aplicação para gerenciar os CRUD's a partir do modelo proposto, queremos analisar suas capacidades em planejamento, modelagem e organização de componentes.

## O Desafio
João tem em seu sistema uma tabela com as roles do seu sistema (ANEXO 1) e uma tabela de categorias (ANEXO 2), porém elas ainda não estão em uso.

João precisa listar e criar os relacionamentos de todas as roles e suas respectivas categorias. Ele definiu que como regra de negócio que uma role pode ter várias categorias e cada categoria cadastrada pode pertencer a mais de uma role.

João também deseja gerenciar (CREATE, READ, UPDATE, DELETE) as categorias e roles do seu sistema.

João gosta muito da visão de Kanban e de utilizar drag n drop para manipular quais categorias devem estar em cada uma das roles.

## Pontos de atenção

Vamos analisar sua capacidade de interpretação da situação problema, como vai se comportar e propor soluções.

O design da aplicação não é um fator importante para este teste, mas é claro que vamos levar em consideração.

## O que vamos avaliar?
1. Funcionamento e método de resolução do problema.
2. Organização do código.
3. Performance do código.
4. Documentação da API.
5. Organização dos componentes
6. Semântica, estrutura, legibilidade, manutenibilidade, escalabilidade do seu código e suas tomadas de decisões.
7. Gerenciamento de estado
8. Históricos de commits do git.

## Entrega

Você deve enviar o link do repositório PUBLICO para o endereço de e-mail:

mateus.silva@diwe.com.br CC: maicon.passos@diwe.com.br

# ANEXO 1 - Tabela de Roles

| id |  RoleName  |  Description  |  Active  |
|--- | ---------- | ---------- | ---------- |
| 1  |  Jogador | Jogador de futebol | true
| 2  |  Colecionador | Colecionador de álbums | true
| 3  |  Cliente | Cliente da banca | false

# ANEXO 2 - Tabela de Categorias

| id |  CategoryName | Active  |
|--- | ---------- | ---------- |
| 1  |  Copa do Mundo | true
| 2  |  Super Herói | true
| 3  |  Futebol | false
